/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shalommedia.floatnote.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.shalommedia.floatnote.helpers.AlarmHelper;
import com.shalommedia.floatnote.helpers.WorkManagerHelper;
import com.shalommedia.floatnote.utils.PrefUtils;

import java.util.concurrent.TimeUnit;

import static com.shalommedia.floatnote.helpers.AlarmHelper.NOTIFICATION_FREQUENCY_MIN;

public class BootReceiver extends BroadcastReceiver {
//
    private final AlarmHelper alarm = new AlarmHelper();
    //private final WorkManagerHelper workManagerHelper =new WorkManagerHelper();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                alarm.cancelAlarm(context);

                long currentMillis = System.currentTimeMillis();
                long notificationFrequencyMs = TimeUnit.MINUTES.toMillis(NOTIFICATION_FREQUENCY_MIN);
                long triggerAtMillis = currentMillis + notificationFrequencyMs;

                PrefUtils.setPrefAlarmTriggerTime(context, triggerAtMillis);
                alarm.setAlarm(context,triggerAtMillis);


                //workManagerHelper.schedule(context);

            }
        }
    }
}
