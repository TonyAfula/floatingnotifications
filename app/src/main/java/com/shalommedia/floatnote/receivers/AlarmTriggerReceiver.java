/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shalommedia.floatnote.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.shalommedia.floatnote.helpers.AlarmHelper;
import com.shalommedia.floatnote.services.PopupService;
import com.shalommedia.floatnote.ui.MainActivity;
import com.shalommedia.floatnote.utils.PrefUtils;

import java.util.concurrent.TimeUnit;

import static com.shalommedia.floatnote.helpers.AlarmHelper.NOTIFICATION_FREQUENCY_MIN;
import static com.shalommedia.floatnote.utils.ChromeTabUtils.URL_2;
import static com.shalommedia.floatnote.utils.ChromeTabUtils.URL_EXTRA;
//https://medium.com/@Chanddru/schedule-tasks-with-alarm-manager-in-modern-android-devices-%EF%B8%8F-1b95bb5e335
public class AlarmTriggerReceiver extends BroadcastReceiver {
    private String TAG = "AlarmTriggerReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        // On Oreo+, it is required to use startForegroundService
        // with implicit promise to start a foreground notification
        // within the ANR timeout. Popup notification
        Intent popupService = new Intent(context, PopupService.class);
        context.stopService(popupService);
        //intent.putExtra("command", "notif"+notifNumber);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(popupService);
        } else {
            context.startService(popupService);
        }

        Intent mainIntent = new Intent(context, MainActivity.class);
        mainIntent.putExtra(URL_EXTRA, URL_2);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        long nextAlarmMillis = getNextAlarmMillis(context);

        AlarmHelper alarm = new AlarmHelper();
        alarm.cancelAlarm(context);
        alarm.setAlarm(context,nextAlarmMillis);
    }



    public long getNextAlarmMillis(Context context) {
        long intervalMillis = TimeUnit.MINUTES.toMillis(NOTIFICATION_FREQUENCY_MIN);
        long nextMillis = PrefUtils.getPrefAlarmTriggerTime(context) + intervalMillis;
        long currentMillis = System.currentTimeMillis();
        // Add interval time in millis until it's greater than current time.
        while (currentMillis > nextMillis)
            nextMillis += intervalMillis;
        return nextMillis;


    }
}
